package service;

import domain.Grade;
import domain.Homework;
import domain.Pair;
import domain.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import repository.GradeXMLRepository;
import repository.HomeworkXMLRepository;
import repository.StudentXMLRepository;
import validation.GradeValidator;
import validation.HomeworkValidator;
import validation.StudentValidator;
import validation.Validator;

import javax.xml.bind.ValidationException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class ServiceTest {

    public static Service service;
    public static Validator<Homework> homeworkValidator;

    @org.junit.jupiter.api.BeforeAll
    public static void setUp() {
        Validator<Student> studentValidator = new StudentValidator();
        // Validator<Homework> homeworkValidator = new HomeworkValidator();
        homeworkValidator = new HomeworkValidator();
        Validator<Grade> gradeValidator = new GradeValidator();

        StudentXMLRepository fileRepository1 = new StudentXMLRepository(studentValidator, "students.xml");
        HomeworkXMLRepository fileRepository2 = new HomeworkXMLRepository(homeworkValidator, "homework.xml");
        GradeXMLRepository fileRepository3 = new GradeXMLRepository(gradeValidator, "grades.xml");

        service = new Service(fileRepository1, fileRepository2, fileRepository3);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void findAllStudents() {
    }

    @org.junit.jupiter.api.Test
    void findAllHomework() {
    }

    @org.junit.jupiter.api.Test
    void findAllGrades() {
    }

    @org.junit.jupiter.api.Test
    void saveValidStudent() {
        Student student = new Student("111", "Student1", 533);
        int result = service.saveStudent(student.getID(), student.getName(), student.getGroup());
        assertTrue(result == 1);
        service.deleteStudent(student.getID());
    }

    @org.junit.jupiter.api.Test
    void saveValidHomework() {
        Homework homework = new Homework("45", "VVSS homework", 6, 2);
        int result = service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                homework.getStartline());
        assertTrue(result == 1);
        service.deleteHomework(homework.getID());
    }

    @org.junit.jupiter.api.Test
    void saveGrade() {
    }

    @org.junit.jupiter.api.Test
    void deleteInvalidStudent() {
        int result = service.deleteStudent("1000");
        assertTrue(result == 0);
    }

    @org.junit.jupiter.api.Test
    void deleteValidHomework() {
        Homework homework = new Homework("111", "VVSS homework", 6, 2);
        int result = service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                homework.getStartline());
        int result2 = service.deleteHomework(homework.getID());
        assertTrue(result2 == 1);
    }

    @org.junit.jupiter.api.Test
    void updateValidStudent() {
        Student student = new Student("1111", "Student1", 533);
        service.saveStudent(student.getID(), student.getName(), student.getGroup());
        int updateResult = service.updateStudent(student.getID(), "Student2", 534);
        assertTrue(updateResult == 1);
        service.deleteStudent(student.getID());
    }

    @org.junit.jupiter.api.Test
    void updateValidHomework() {
        Homework homework = new Homework("4555", "VVSS homework", 6, 2);
        service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                homework.getStartline());
        int updateRresult = service.updateHomework(homework.getID(), "A", 6, 2);
        assertTrue(updateRresult == 1);
        service.deleteHomework(homework.getID());
    }

    @org.junit.jupiter.api.Test
    void extendDeadline() {
        Homework homework = new Homework("455551", "VVSS homework", 6, 2);
        service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                homework.getStartline());
        int result = service.extendDeadline(homework.getID(), 1);
       // assertTrue(result == 1);
        // service.deleteHomework(homework.getID());
    }

    @org.junit.jupiter.api.Test
    void extendInvalidDeadline() {
        Homework homework = new Homework("45555", "VVSS homework", 6, 2);
        service.saveHomework(homework.getID(), homework.getDescription(), homework.getDeadline(),
                homework.getStartline());
        int result = service.extendDeadline(homework.getID(), 1);

    }

    @org.junit.jupiter.api.Test
    void createStudentFile() {
    }
}